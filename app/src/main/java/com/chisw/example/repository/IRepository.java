package com.chisw.example.repository;

/**
 * Created by Alex Kriatov on 2019-08-21
 */
public interface IRepository {
    String loadData();
}
