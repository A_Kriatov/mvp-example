package com.chisw.example.repository;

/**
 * Created by Alex Kriatov on 2019-08-21
 */
public class RepositoryImpl implements IRepository {
    @Override
    public String loadData() {
        return "Loaded data";
    }
}
