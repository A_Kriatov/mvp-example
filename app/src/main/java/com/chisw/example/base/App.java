package com.chisw.example.base;

import android.app.Application;

import com.chisw.example.repository.IRepository;
import com.chisw.example.repository.RepositoryImpl;

/**
 * Created by Alex Kriatov on 2019-08-21
 */
public class App extends Application implements Injection {

    private IRepository repository;

    @Override
    public void onCreate() {
        super.onCreate();
        repository = new RepositoryImpl();
    }

    @Override
    public IRepository injectRepository() {
        return repository;
    }
}
