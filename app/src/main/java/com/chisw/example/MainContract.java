package com.chisw.example;


/**
 * Created by Alex Kriatov on 2019-08-21
 */
public interface MainContract {
   interface View{
       void showProgress();
       void hideProgress();
       void updateUi(String data);
   }

   interface Presenter{
       void attachView(View view);

   }
}
