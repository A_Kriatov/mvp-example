package com.chisw.example.base;

import com.chisw.example.repository.IRepository;

/**
 * Created by Alex Kriatov on 2019-08-21
 */
public interface Injection {
    IRepository injectRepository();
}
