package com.chisw.example;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chisw.example.base.Injection;

public class MainActivity extends AppCompatActivity implements MainContract.View {


    private TextView textView;
    private ProgressBar progress;
    private MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = findViewById(R.id.progress);
        textView = findViewById(R.id.tvMain);
        presenter = new MainPresenter((Injection) getApplicationContext());
        presenter.attachView(this);
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void updateUi(String data) {
        textView.setText(data);
    }
}
