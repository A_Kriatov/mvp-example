package com.chisw.example;

import android.os.Handler;

import com.chisw.example.base.Injection;
import com.chisw.example.repository.IRepository;

import java.lang.ref.WeakReference;

/**
 * Created by Alex Kriatov on 2019-08-21
 */
public class MainPresenter implements MainContract.Presenter {

    private IRepository repository;
    private WeakReference<MainContract.View> view;

    MainPresenter(Injection injection){
        repository = injection.injectRepository();
    }

    @Override
    public void attachView(final MainContract.View view) {
        this.view = new WeakReference(view);
        view.showProgress();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view.hideProgress();
                view.updateUi(repository.loadData());
            }
        }, 5000L);
    }
}
